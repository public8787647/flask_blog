"""topic del and add subs

Revision ID: 5966780addfc
Revises: 1ddb881b0f44
Create Date: 2022-12-09 00:26:17.820239

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '5966780addfc'
down_revision = '1ddb881b0f44'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('topics')
    with op.batch_alter_table('topic', schema=None) as batch_op:
        batch_op.drop_index('ix_topic_topicname')

    op.drop_table('topic')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('topic',
    sa.Column('id', sa.INTEGER(), nullable=False),
    sa.Column('topicname', sa.VARCHAR(length=64), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    with op.batch_alter_table('topic', schema=None) as batch_op:
        batch_op.create_index('ix_topic_topicname', ['topicname'], unique=False)

    op.create_table('topics',
    sa.Column('usedtopic_id', sa.INTEGER(), nullable=True),
    sa.Column('topicname_id', sa.INTEGER(), nullable=True),
    sa.ForeignKeyConstraint(['topicname_id'], ['topic.id'], ),
    sa.ForeignKeyConstraint(['usedtopic_id'], ['post.id'], )
    )
    # ### end Alembic commands ###
